#!/usr/bin/env python3

import json
import re
import urllib.parse
import urllib.request

class NameAPI:
    "An interface to the name.com API"

    BASE_PATH = 'api'
    API_GET_HEADER =  {'Accept':       'application/json'}
    API_POST_HEADER = {'Content-Type': 'application/json'}

    RESP_CODE_MAP = {100:   'Command Successful',
                     203:   'Required Parameter Missing',
                     204:   'Parameter Value Error',
                     211:   'Invalid Command URL',
                     221:   'Authorization Error',
                     240:   'Command Failed',
                     250:   'Unexpected Error (Exception)',
                     251:   'Authentication Error',
                     260:   'Insufficient Funds',
                     261:   'Unable to Authorize Funds'}

    def __init__(self, account, token, api_url='https://api.name.com/'):
        self.api_url = api_url
        self.account = account
        self.token = token

        self.session_token = self.login()
        self.auth_header = {'Api-Session-Token': self.session_token}

        self.get_opener = urllib.request.build_opener()
        self.get_opener.addheaders = self.merge_dicts(self.API_GET_HEADER, self.auth_header).items()
        self.post_opener = urllib.request.build_opener()
        self.post_opener.addheaders = self.merge_dicts(self.API_GET_HEADER, self.API_POST_HEADER, self.auth_header).items()

    def __del__(self):
        return self.logout()

    def merge_dicts(self, *dicts):
        "Merge dicts together, with later entries overriding earlier ones."
        merged = {}
        for d in dicts:
            merged.update(d)
        return merged

    def path_join(self, *paths):
        "Join some paths together"
        return re.sub(r'\/{2,}', '/', '/'.join(paths))

    def gen_url(self, path):
        return urllib.parse.urljoin(self.api_url, self.path_join(self.BASE_PATH, path))

    def _get(self, path):
        "Make a generic GET request"
        url = self.gen_url(path)
        req = urllib.request.Request(url)
        resp = self.get_opener.open(req)
        return json.loads(resp.read().decode('utf-8'))

    def _post(self, path, data, headers={}):
        # Content-Type isn't being pulled from post_opener properly, so set it here when
        # creating the request
        url = self.gen_url(path)
        merged_headers = self.merge_dicts(self.API_POST_HEADER, headers)
        req = urllib.request.Request(url, data=data, headers=merged_headers)
        resp = self.post_opener.open(req)
        return json.loads(resp.read().decode('utf-8'))

    def hello(self):
        return self._get('hello')

    def login(self):
        "Return a session token"
        url = self.gen_url('login')
        payload = {'username':  self.account,
                   'api_token': self.token}
        encoded_payload = json.dumps(payload).encode('utf-8')
        resp = urllib.request.urlopen(url, data=encoded_payload)
        resp_data = json.loads(resp.read().decode('utf-8'))
        return resp_data['session_token']

    def logout(self):
        "End session"
        return self._get('logout')

    def list_domains(self):
        "Return a list of domains"
        resp_data = self._get('domain/list')
        if not 'domains' in resp_data:
            return []
        if isinstance(resp_data['domains'], dict):
            return list(resp_data['domains'].keys())
        else:
            return resp_data['domains']

    def list_records(self, domain):
        "Return DNS records for domain"
        return self._get('dns/list/{domain}'.format(domain=domain))

    def create_record(self, domain, name, type, content, ttl=300, priority=10):
        url = 'dns/create/{domain}'.format(domain=domain)
        payload = {'hostname':  name,
                   'type':      type,
                   'content':   content,
                   'ttl':       ttl,
                   'priority':  priority,
                  }
        encoded_payload = json.dumps(payload).encode('utf-8')
        return self._post(url, data=encoded_payload)

    def delete_record(self, domain, record_id):
        url = 'dns/delete/{domain}'.format(domain=domain)
        payload = {'record_id': record_id}
        encoded_payload = json.dumps(payload).encode('utf-8')
        return self._post(url, data=encoded_payload)

    def match_record(self, domain, name, type='A'):
        "Return records that match the given parameters"
        return [r for r in self.list_records(domain)['records']
                if r['name'].startswith(name) and r['type'] == type]

    def update_records(self, domain, name, new_data, type='A'):
        updated_records = []
        interesting_keys = ('name', 'type', 'content', 'ttl', 'priority')
        for existing_record in self.match_record(domain, name, type):
            existing_data = {}
            for key in interesting_keys:
                if key in existing_record:
                    existing_data[key] = existing_record[key]
            new_record = self.merge_dicts(existing_data, new_data, {'name': name})

            # Remove old record
            self.delete_record(domain, existing_record['record_id'])

            # Create record with new data
            updated_records.append(self.create_record(domain=domain, **new_record))

        return updated_records

