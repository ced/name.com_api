#!/usr/bin/env python3

import json
from pprint import pprint

from name_com_api import NameAPI

if __name__ == '__main__':
    domain = 'slimymelon.org'

    with open('name_com_token.json', 'r') as fh:
        creds = json.load(fh)

    client = NameAPI(
            account=creds['account'],
            token=creds['token'],
            )

    pprint(client.hello())
    #pprint(client.list_domains())
    pprint(client.list_records(domain))
    #pprint(client.create_record(domain, name='banana', type='A', content='1.2.3.4', ttl=300, priority=10))
    #pprint(client.delete_record(domain, record_id='267634866'))
    #pprint(client.match_record(domain, name='banana'))
    #pprint(client.update_records(domain, name='banana', type='A', new_data={'content': '5.6.7.8'}))
    #for record in client.match_record(domain, name='banana'):
    #    pprint(client.delete_record(domain, record_id=record['record_id']))
